#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ(2.61)

dnl Name your program here
m4_define([full_package_name], [mysql_printf])

dnl These three define the plug-in version number
m4_define([major_version], [1])
m4_define([minor_version], [2])
m4_define([micro_version], [2])
# Comment build_version for stable release
# m4_define([build_version], [dev])

m4_ifdef([build_version], [
	  m4_define([version],
		    [major_version.minor_version.micro_version-build_version])
	  ],
	  [
	   m4_define([version],
		     [major_version.minor_version.micro_version])
	   ]
	  )



m4_define([bug_report_address], [matteo.quintiliani@ingv.it])

# AC_INIT(FULL-PACKAGE-NAME, VERSION, BUG-REPORT-ADDRESS)
AC_INIT([full_package_name], [version], [bug_report_address])
AC_CONFIG_SRCDIR([src/mysql_printf.c])
AC_CONFIG_HEADER([config.h])

AC_CANONICAL_SYSTEM
AM_INIT_AUTOMAKE
# Default argument for AM_MAINTAINER_MODE is disable
AM_MAINTAINER_MODE([disable])

# Checks for programs.
AC_PROG_CC
# Check program for symbolic link
AC_PROG_LN_S

AC_ARG_WITH(
	    [mysql-config],
	    AS_HELP_STRING(
			   [--with-mysql-config=FILE],
			   [File name for utility 'msql_config']
			   ),
			   [with_mysql_config="$withval"],
			   [with_mysql_config="mysql_config"]
	   )

MYSQL_CONFIG_PROG="$with_mysql_config"
AC_CHECK_PROG(MYSQL_CONFIG, "$MYSQL_CONFIG_PROG", "yes", "no")

AS_IF([test "$MYSQL_CONFIG" = "yes"],
      [
       CFLAGS="$CFLAGS `$MYSQL_CONFIG_PROG --cflags`"
       LDFLAGS="$LDFLAGS `$MYSQL_CONFIG_PROG --libs`"
       ],
       [ AC_MSG_ERROR([
	$MYSQL_CONFIG_PROG not found!
	Use the option --with-mysql-config=/path/mysql_config]) ]
       )
    
AC_ARG_ENABLE([libconfig],
	      [AS_HELP_STRING([--disable-libconfig], [disable using libconfig])],
	      [],
	      [enable_libconfig=yes]
)

AC_CHECK_PROG(PKG_CONFIG, "pkg-config", "yes", "no")
ATLEAST_LIBCONFIGVERSION="1.3.2"
# N.B. break token "version"
PKG_OPT_ATLEAST_LIBCONFIGVERSION="--atleast-ver\
sion=$ATLEAST_LIBCONFIGVERSION"

AS_IF([test "x$enable_libconfig" != xno],
    [ AS_IF([test "$PKG_CONFIG" = "yes"],
	  [
	   AS_IF([pkg-config --exists libconfig],
		 [
		  LIBCONFING_VERSION_FOUND=`pkg-config --modversion libconfig`
		  AS_IF([pkg-config $PKG_OPT_ATLEAST_LIBCONFIGVERSION libconfig],
			[
			 CFLAGS="$CFLAGS -DHAVE_LIBCONFIG"
			 CFLAGS="$CFLAGS `pkg-config --cflags libconfig`"
			 LDFLAGS="$LDFLAGS `pkg-config --libs libconfig`"
			 AC_MSG_NOTICE([libconfig $LIBCONFING_VERSION_FOUND found!])
			 ],
			 [ AC_MSG_ERROR([
		      Found libconfig $LIBCONFING_VERSION_FOUND. libconfig $ATLEAST_LIBCONFIGVERSION or greater is required!
			     ])
			 ]
			 )
		  ],
		  [
		   AC_MSG_WARN([libconfig not found! Check PKG_CONFIG_PATH.])
		   AS_IF([test "x$PKG_CONFIG_PATH" != "x"],
			 [ AC_MSG_WARN([PKG_CONFIG_PATH=$PKG_CONFIG_PATH]) ],
			 [ AC_MSG_WARN([PKG_CONFIG_PATH is empty]) ]
			 )
		   ]
		  )
	   ],
	   [ AC_MSG_WARN([
	    pkg-config not found!
	    no check for libconfig.]) ]
	   )
    ],
    [
     AC_MSG_WARN([libconfig feature has been disabled!])
     ]
)

# Checks for libraries.

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([stdlib.h string.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST

# mtheo
AC_ARG_VAR([PKG_CONFIG_PATH],[directory where pkg-config looks for *.pc files])

# Checks for library functions.
AC_CHECK_FUNCS([strdup])

AC_CONFIG_FILES([Makefile
		 src/Makefile])
AC_OUTPUT
