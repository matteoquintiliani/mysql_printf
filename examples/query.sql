SELECT
    station.id_inter,
    network.net_code,
    substr(channel.id_cha,-1,1) as subcomp,
    substr(channel.id_cha,-3,3) as subcha,
    FLOOR(station.latitude) as lat,
    ROUND(((station.latitude-FLOOR(station.latitude))*60), 4) as lat_s,
    IF(station.latitude > 0, 'N', 'S'),
    FLOOR(station.longitude) as lon,
    ROUND(((station.longitude-FLOOR(station.longitude))*60), 4) as lon_s,
    IF(station.longitude > 0, 'E', 'W'),
    station.altitude * 1.0,
    '1 0.00  0.00  0.00  0.00 1  0.00',
    IF(channel.location IS NULL,'--',channel.location),
    station.name
FROM station,channel,network
WHERE (
    channel.fk_network=network.id
    OR (channel.fk_network IS NULL AND network.id=1)
    )
    AND channel.fk_station=station.id
    AND channel.end_time > NOW()
ORDER BY station.id_inter,subcha,network.net_code;

