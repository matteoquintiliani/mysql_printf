#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <config.h>

#include <mysql.h>

#include <getopt.h>
#include <time.h>

#ifdef HAVE_LIBCONFIG
#include <libconfig.h>
/*
 * http://www.hyperrealm.com/libconfig/
 * http://www.hyperrealm.com/libconfig/libconfig_manual.html
 * http://www.hyperrealm.com/libconfig/libconfig-1.3.2.tar.gz
 * 	tar xvfz libconfig-1.3.2.tar.gz
 * 	./configure --disable-cxx && make
 */
#endif


#define MAX_LEN_QUERY 4096 * 8
#define MAX_LEN_LINE 1024

#define MAX_LEN_FILENAME 1024

#define STR_NULL(s) ( (s == NULL)? "<null>" : s )

#ifdef HAVE_LIBCONFIG
/* File that contains query */
#define config_filename_default "mysql_printf.conf"
#endif

#ifdef __WIN32__
#define SEPDIR '\\'
#define outputdir_default ".\\"
#else
#define SEPDIR '/'
#define outputdir_default "./"
#endif

#define DELIMITER_CHAR_DEFAULT ';'

#define DEBUG_PREFIX_COMMENT "#"

typedef struct {
	char *str_sql;
	char *filename_sql;
	char *str_format;
	char delimiter;
	char *filename_format;
	char *outputdir;
#ifdef HAVE_LIBCONFIG
	char *config_filename;
#endif
	char *dbconfname;
	char *username;
	char *password;
	char *dbname;
	char *hostname;
	long dbport;
	int flag_debugmsg;
	char *debugpref;
	int flag_multifiles;
	int flag_appendfile;
} MYSQL_FORMAT_PARAMS;

const MYSQL_FORMAT_PARAMS MYSQL_FORMAT_PARAMS_DEFAULT =
{
	NULL,
	NULL,
	NULL,
	DELIMITER_CHAR_DEFAULT,
	NULL,
	outputdir_default,
#ifdef HAVE_LIBCONFIG
	config_filename_default,
#endif
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	0,
	0,
	NULL,
	0,
	0
};

int mysql_printf_getopt_long(int argc, char **argv, MYSQL_FORMAT_PARAMS *params);
char *read_string_from_file(char *fname);
char *convert_escape_sequence(char *in_str);
int fprintf_str_format(FILE *f, char *str_format, char delimiter,  MYSQL_ROW *row, int num_fields, int i_f_start);

void mysql_printf_author_support();
void mysql_printf_help_info();
void mysql_printf_version();
void mysql_printf_supports();
void mysql_printf_usage(struct option long_options[]);
int mysql_printf_check_params(MYSQL_FORMAT_PARAMS *params);


char *mysql_printf_gnu_getcwd ();
int mysql_printf_dir_exists (char *dirname);

#ifdef HAVE_LIBCONFIG
int config_lookup_string_db (config_t *config, const char *dbconfname, char *path, const char **ret);
int config_lookup_int_db (config_t *config, const char *dbconfname, char *path, long *ret);
#endif


int main(int argc, char **argv)
{
	MYSQL *mysql;
	MYSQL_RES *res;
	MYSQL_ROW row;
	MYSQL_FIELD *field;

	char filename_output[MAX_LEN_FILENAME];
	FILE *fout;

	/* Connection parameters */
	const char *hostname = NULL;
	const char *username = NULL;
	const char *password = NULL;
	const char *dbname = NULL;
	long dbport = 0;

	const char *sepline_major = "================================================================================================";
	const char *sepline_minor = "------------------------------------------------------------------------------------------------";

	char *query = NULL;
	char *str_format = NULL;

	int num_fields;
	unsigned long *lengths;

	int i;
	int query_length;
	int status = 0;

	time_t now = time(NULL);

#ifdef HAVE_LIBCONFIG
#define MAX_LEN_FILENAME 1024
	const char *dbconfname = NULL;
	char config_fullpathname[MAX_LEN_FILENAME] = "";
	char *config_pathame = NULL;
#define ENV_CONFIG_PATHNAME "MYSQL_FORMAT_DIR"
	config_t config;
	int flag_use_libconfig = 0;
#endif

	MYSQL_FORMAT_PARAMS params;

	/* Initialize params from argument values */
	if(mysql_printf_getopt_long(argc, argv, &params) != 0) {
		return -1;
	}

	/* Check consistency of params */
	if(mysql_printf_check_params(&params) != 0) {
		return -1;
	}

	/* Init query */
	if(params.filename_sql) {
		query = read_string_from_file(params.filename_sql);
		if(  query == NULL ) {
			fprintf(stderr, "Error: '%s' file not found!\n", params.filename_sql);
			return -1;
		}
	} else if(params.str_sql) {
		query = strdup(params.str_sql);
	} else {
		fprintf(stderr, "Neither argument <sqlfile> nor <sqlstr> are defined!\n");
		return -1;
	}

	/* Init str_format */
	if(params.filename_format) {
		if( (str_format = read_string_from_file(params.filename_format)) == NULL ) {
			fprintf(stderr, "Error: file '%s' is empty\n", params.filename_format);
			return -1;
		}
	} else if(params.str_format) {
		str_format = strdup(params.str_format);
	}

	/* Start comment in output */
	if(params.flag_debugmsg) {
		printf("%s %s\n", params.debugpref, sepline_major);
	}

	if(params.flag_debugmsg) {
		printf("%s %s-%s   libconfig: %s\n", params.debugpref,
				PACKAGE_NAME, PACKAGE_VERSION,
#ifdef HAVE_LIBCONFIG
				"yes"
#else
				"no"
#endif
				);

		printf("%s Date: %s", params.debugpref, ctime(&now));
	}

	if(params.hostname || params.dbname) {
		hostname = params.hostname;
		username = params.username;
		dbname = params.dbname;;
		password = params.password;
		dbport = params.dbport;
		if(!hostname && !dbname && !username) {
			fprintf(stderr, "Error: hostname, dbname are required!");
#ifdef HAVE_LIBCONFIG
			fprintf(stderr, "       Alternatively you can use libconfig file configuration.!");
#endif
			return -1;
		}

	} else {
#ifdef HAVE_LIBCONFIG
		flag_use_libconfig = 1;

		/* Init configuration file structure */
		config_init(&config);

		if(strcmp(params.config_filename, config_filename_default) != 0) {
			snprintf(config_fullpathname, MAX_LEN_FILENAME, "%s", params.config_filename);
		} else if ( (config_pathame = getenv(ENV_CONFIG_PATHNAME)) ) {
			snprintf(config_fullpathname, MAX_LEN_FILENAME, "%s/%s", config_pathame, params.config_filename);
		} else {
			fprintf(stderr, "Warning: environment variable %s not found!\nLooking for %s into the current directory... ...good luck!\n",
					ENV_CONFIG_PATHNAME, params.config_filename);
			snprintf(config_fullpathname, MAX_LEN_FILENAME, "./%s", params.config_filename);
		}

		config_read_file(&config, config_fullpathname);
		dbconfname = params.dbconfname;
		if(!dbconfname) {
			config_lookup_string (&config, "default.db", &dbconfname);
		}
		if(!dbconfname) {
			fprintf(stderr, "Error reading variable 'dbconfname' from configuration file %s.\n", config_fullpathname);
			return -1;
		}
		config_lookup_string_db (&config, dbconfname, "hostname", &hostname);
		config_lookup_string_db (&config, dbconfname, "username", &username);
		config_lookup_string_db (&config, dbconfname, "password", &password);
		config_lookup_string_db (&config, dbconfname, "dbname", &dbname);
		config_lookup_int_db (&config, dbconfname, "dbport", &dbport);

		if(!hostname) {
			fprintf(stderr, "Error reading variable '%s.hostname' from configuration file %s.\n", dbconfname, config_fullpathname);
			return -1;
		}

		if(!username) {
			fprintf(stderr, "Error reading variable '%s.username' from configuration file %s.\n", dbconfname, config_fullpathname);
			return -1;
		}

		if(!password) {
			fprintf(stderr, "Error reading variable '%s.password' from configuration file %s.\n", dbconfname, config_fullpathname);
			return -1;
		}

		if(!dbname) {
			fprintf(stderr, "Error reading variable '%s.dbname' from configuration file %s.\n", dbconfname, config_fullpathname);
			return -1;
		}
#endif
	}
	/* Init struct mysql */
	mysql = mysql_init(NULL);

	/* Connect to MySQL server */
	if(!mysql_real_connect(mysql,hostname,username,password,dbname,dbport,NULL,CLIENT_MULTI_STATEMENTS)) {
		fprintf(stderr, "Error connecting to %s:%ld. (%s)\n", hostname, dbport, mysql_error(mysql));
		return -1;
	}

	if(mysql_real_query(mysql,query,(unsigned int)strlen(query)) != 0) {
		fprintf(stderr, "Error: %d '%s' '%s' in query:\n'%s'\n", mysql_errno(mysql), mysql_error(mysql), mysql_sqlstate(mysql), query);
		return -1;
	}

	do {
		/* Init res */
		res = mysql_use_result(mysql);

		if(res) {

			if(params.flag_debugmsg) {
#ifdef HAVE_LIBCONFIG
				printf("%s dbconfname: %s\n", params.debugpref, dbconfname);
#endif
				printf("%s hostname: %s; dbport: %ld; dbname: %s; username: %s.\n", params.debugpref, hostname, dbport, dbname, username);
				printf("%s mysql_get_client_info()   : %s\n", params.debugpref, mysql_get_client_info());
				printf("%s mysql_get_client_version(): %ld\n", params.debugpref, mysql_get_client_version());

				/* Print server info after mysql_real_connect() */
				printf("%s mysql_get_host_info()     : %s\n", params.debugpref, mysql_get_host_info(mysql));
				printf("%s mysql_get_proto_info()    : %d\n", params.debugpref, mysql_get_proto_info(mysql));
				printf("%s mysql_get_server_info()   : %s\n", params.debugpref, mysql_get_server_info(mysql));
				printf("%s mysql_get_server_version(): %ld\n", params.debugpref, mysql_get_server_version(mysql));
				printf("%s mysql_stat()              : %s\n", params.debugpref, mysql_stat(mysql));

				printf("%s file_sql   : %s\n", params.debugpref, params.filename_sql);
				printf("%s file_format: %s\n", params.debugpref, params.filename_format);
				printf("%s outputdir: %s\n", params.debugpref, params.outputdir);

				/* Print query with # after each newline */
				query_length = strlen(query);
				printf("%s %s\n", params.debugpref, sepline_minor);
				printf("%s Query : ", params.debugpref);
				for(i=0; i < query_length; i++) {
					printf("%c", query[i]);
					if(query[i] == '\n') {
						printf("%s ", params.debugpref);
					}
				}
				printf("\n");

				printf("%s Fields: ", params.debugpref);
				while((field = mysql_fetch_field(res)))
				{
					printf("{%s}%c", field->name, params.delimiter);
				}
				printf("\n");
			}

			/* End comment in output */
			if(params.flag_debugmsg) {
				printf("%s %s\n", params.debugpref, sepline_major);
			}

			num_fields = mysql_num_fields(res);
			while( (row = mysql_fetch_row(res)) ) {
				lengths = mysql_fetch_lengths(res);
				/*
					 for(i = 0; i < num_fields; i++)
					 {
					 printf("[%.*s] ", (int) lengths[i], row[i] ? row[i] : "NULL");
					 }
					 printf("\n");
					 */

				if(params.flag_multifiles) {
					snprintf(filename_output, MAX_LEN_FILENAME, "%s%c%s", params.outputdir, SEPDIR, STR_NULL(row[0]));
					fout = fopen(filename_output, params.flag_appendfile ? "at" : "wt");
					if(fout) {
						fprintf_str_format(fout, str_format, params.delimiter, &row, num_fields, 1);
						fclose(fout);
					} else {
						fprintf(stderr, "Error: opening file '%s'\n", filename_output);
						return -1;
					}
				} else {
					fprintf_str_format(stdout, str_format, params.delimiter, &row, num_fields, 0);
				}


			}
			/* Free res */
			mysql_free_result(res);

		} else  /* no result set or error */
		{
			if (mysql_field_count(mysql) == 0) {
				fprintf(stderr, "%lld rows affected\n",
						mysql_affected_rows(mysql));
			} else  /* some error occurred */
			{
				fprintf(stderr, "Could not retrieve result set\n");
				break;
			}
		}
		/* more results? -1 = no, >0 = error, 0 = yes (keep looping) */
		if ((status = mysql_next_result(mysql)) > 0)
			printf("Could not execute statement\n");

	} while (status==0);

	if(str_format) {
		free(str_format);
	}
	if(query) {
		free(query);
	}

	/* Free struct mysql */
	mysql_close(mysql);

#ifdef HAVE_LIBCONFIG
	if(flag_use_libconfig) {
		/* Free configuration file structure */
		config_destroy(&config);
	}
#endif

	return 0;
}



char *read_string_from_file(char *fname) {
	char *ret = NULL;
	char final_string[MAX_LEN_QUERY];
	char line[MAX_LEN_LINE];
	FILE *f = NULL;
	f = fopen(fname, "rt");
	if(f) {
		final_string[0] = 0;
		while( (fgets(line, MAX_LEN_LINE, f)) ){
			if (strlen(line) + 1 > sizeof(final_string) - strlen(final_string)) {
				fprintf(stderr, "final_string from file '%s' would be truncated\n", fname);
			}
			strncat(final_string, line, sizeof(final_string) - strlen(final_string) - 1);
		}
		if(final_string[0] != 0) {
			ret = strdup(final_string);
		}
		fclose(f);
	}
	return ret;
}


/* http://stackoverflow.com/questions/17015970/how-does-c-compiler-convert-escape-sequence-to-actual-bytes */
char *convert_escape_sequence(char *in_str) {
	char *p=in_str;
	size_t len=strlen(in_str);
	int num;
	int numlen;

	while (NULL!=(p=strchr(p,'\\'))) {
		numlen=1;
		switch (p[1]) {
			case '\\':
				break;
			case 'r':
				*p = '\r';
				break;
			case 'n':
				*p = '\n';
				break;
			case 't':
				*p = '\t';
				break;
			case 'v':
				*p = '\v';
				break;
			case 'a':
				*p = '\a';
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
				numlen = sscanf(p,"%o",&num);
				*p = (char)num;
				break;
			case 'x':
				numlen = sscanf(p,"%x",&num);
				*p = (char) num;
				break;
		}
		num = p - in_str + numlen;
		p++;
		memmove(p,p+numlen,len-num );
	}
	return in_str;
}

/* 
 * str_format can contains definition of %s like printf, for example:
 * %s
 * %-s
 * %10s
 * %-3s
 */
int fprintf_str_format(FILE *f, char *str_format, char delimiter, MYSQL_ROW *row, int num_fields, int i_f_start) {
	int ret = 0;
	char *sub_format = NULL;
	int i_f = 0;          /* index for field */
	int i_s = 0;          /* index for start field  */
	int i_e = 0;          /* index for end field */
	int i = 0;
	int len = 0;
	char *p = str_format;
	int go_again;
	int i_field;
	int mult = 3;
	int tmp_i_s;          /* temporary inde for start field
													 for validating syntax after character %*/

	i_f = i_f_start;
	i_s = 0;

	if(!str_format) {
		p = (char *) malloc(sizeof(char) * mult * (num_fields + 1));
		i_field = 0;
		while(i_field < num_fields) {
			p[(i_field*mult) + 0] = '%';
			p[(i_field*mult) + 1] = 's';
			p[(i_field*mult) + 2] = delimiter;
			i_field++;
		}
		p[(i_field*mult)] = '\n';
		p[(i_field*mult)+1] = 0;
	}

	p = convert_escape_sequence(p);

	len = strlen(p);

	sub_format = (char *) malloc(sizeof(char)*(len+1)); 

	/* main loop */
	while(i_f < num_fields  &&  i_s < len) {

		/* print characters until find a '%...s' */
		/* set i_s */
		do {

			while(i_s < len  &&  p[i_s] != '%') {
				fprintf(f, "%c", p[i_s]);
				i_s++;
			}

			go_again = 0;
			if(i_s+1 < len) {
				/* validate sintax after the character % */
				tmp_i_s = i_s+1;
				if(p[tmp_i_s] == '-') {
					tmp_i_s++;
				}
				while(tmp_i_s < len &&
						( p[tmp_i_s] == 's' ||  (p[tmp_i_s] >= '0' && p[tmp_i_s] <= '9' ) )
						) {
					tmp_i_s++;
				}

				if( tmp_i_s <= len && p[tmp_i_s-1] == 's' ) {
					/* it is a syntax like '%...s' */
					/* set i_e */
					i_e = tmp_i_s;
				} else {
					/* it is not a syntax like '%...s' */
					go_again = 1;
					fprintf(f, "%c", p[i_s]);
					i_s++;
				}
			}
		} while(go_again);

		/* copy sub string in sub_format */
		i=i_s;
		while(i < i_e) {
			sub_format[i - i_s] = p[i];
			i++;
		}
		sub_format[i - i_s] = 0;

		/* print i_f-th field */
		fprintf(f, sub_format, STR_NULL((*row)[i_f]));

		i_s = i_e;

		i_f++;
	} /* end main loop */

	if(i_f < num_fields) {
		fprintf(stderr, "Error: i_f < num_fields    %d < %d\n", i_f, num_fields);
	}

	/* Flush remaining characters */
	if(i_s < len) {
		/* set i_s */
		while(i_s < len) {
			if(p[i_s] == 's'  &&  p[i_s-1]=='%') {
				fprintf(stderr, "Warning: %%s appear at pos %d but there is not corresponding field from query result!\n", i_s);
			}
			fprintf(f, "%c", p[i_s]);
			i_s++;
		}
	}

	if (sub_format) {
		free (sub_format);
	}
	if (!str_format && p) {
		free (p);
	}
	return ret;
}


#ifdef HAVE_LIBCONFIG
int config_lookup_string_db (config_t *config, const char *dbconfname, char *path, const char **ret) {
	char app_str[1024];
	snprintf(app_str, 1024, "%s.%s", dbconfname, path);
	return config_lookup_string(config, app_str, ret);
}

int config_lookup_int_db (config_t *config, const char *dbconfname, char *path, long *ret) {
	char app_str[1024];
	snprintf(app_str, 1024, "%s.%s", dbconfname, path);
	return config_lookup_int(config, app_str, ret);
}
#endif


int mysql_printf_getopt_long(int argc, char **argv, MYSQL_FORMAT_PARAMS *params) {
	int ret_errors = 0;

	int i;
	char one_time_option[255];
	int c;

	struct option long_options[] =
	{
		/* These options set a flag. */
		/* It is not safe use reference to params in this way */
		/* {"verbose",        no_argument,       &(params->flag_verbose), 1}, */
		/* {"quiet",          no_argument,       &(params->flag_verbose), 0}, */
		/* These options don't set a flag.
		 *                   We distinguish them by their indices. */
		{"host",         required_argument, NULL, 'h'},
		{"user",         required_argument, NULL, 'u'},
		{"password",     required_argument, NULL, 'p'},
		{"database",     required_argument, NULL, 'd'},
		{"dbport",       required_argument, NULL, 'P'},

#ifdef HAVE_LIBCONFIG
		{"fileconf",     required_argument, NULL, 'C'},
		{"dbconfname",   required_argument, NULL, 'D'},
#endif

		{"fmtstr",    required_argument, NULL, 'F'},
		{"fmtfile",   required_argument, NULL, 'f'},
		{"delimiter",    required_argument, NULL, 'e'},
		{"sqlstr",       required_argument, NULL, 'S'},
		{"sqlfile",      required_argument, NULL, 's'},
		{"outputdir",    required_argument, NULL, 'o'},

		/* Following are flags */
		{"multifiles",   no_argument,       NULL, 'm'},
		{"appendfile",   no_argument,       NULL, 'a'},
		{"debugmsg",     no_argument,       NULL, 'g'},
		{"debugpref",    required_argument, NULL, 'x'},
		{"help",         no_argument,       NULL, 'H'},
		{"version",      no_argument,       NULL, 'V'},
		{0, 0, 0, 0}
	};

	int option_index = 0;

#define MAX_LEN_OPTSTR 256
	char optstr[MAX_LEN_OPTSTR] = "h:u:p:d:P:";
#ifdef HAVE_LIBCONFIG
	strcat(optstr, "C:D:");
#endif
	strcat(optstr, "F:f:e:S:s:o:x:magHV");

	/* if there is no arguments provided */
	if(argc == 1) {
		mysql_printf_version();
		mysql_printf_help_info();
		mysql_printf_author_support();
		exit(1);
	}

	/* getopt_long stores the option index here. */
	/* init array for checking one time option */
	for(i=0; i<255; i++) {
		one_time_option[i] = 0;
	}

	/* init params */
	memcpy(params, &MYSQL_FORMAT_PARAMS_DEFAULT, sizeof(MYSQL_FORMAT_PARAMS_DEFAULT));

	params->debugpref = DEBUG_PREFIX_COMMENT;

	while ( (c = getopt_long (argc, argv, optstr, long_options, &option_index)) != -1) {

		/* BE CAREFUL if use synonym options !!! */
		one_time_option[c]++;

		if(one_time_option[c] > 1) {
			ret_errors++;
			fprintf(stderr, "Replicated option -%c (value %s)\n", c, STR_NULL(optarg));
		} else {
			switch (c)
			{
				case 0:
					/* If this option set a flag, do nothing else now. */
					if (long_options[option_index].flag != 0)
						break;
					fprintf(stderr, "option %s",
							STR_NULL(long_options[option_index].name));
					if (optarg) {
						fprintf(stderr, " with arg %s", STR_NULL(optarg));
					}
					fprintf(stderr, "\n");
					break;

#ifdef HAVE_LIBCONFIG
				case 'C':
					params->config_filename =  optarg;
					break;

				case 'D':
					params->dbconfname =  optarg;
					break;
#endif

				case 'h':
					params->hostname =  optarg;
					break;

				case 'u':
					params->username =  optarg;
					break;

				case 'p':
					params->password =  optarg;
					break;

				case 'd':
					params->dbname =  optarg;
					break;

				case 'P':
					params->dbport =  atol(optarg);
					break;

				case 'F':
					params->str_format =  optarg;
					if(params->filename_format) {
						ret_errors++;
						fprintf(stderr, "Error: option -f, -F should not be used together.\n");
					}
					break;

				case 'f':
					params->filename_format =  optarg;
					if(params->str_format) {
						ret_errors++;
						fprintf(stderr, "Error: option -f, -F should not be used together.\n");
					}
					break;

				case 'e':
					if(strlen(optarg) != 1) {
						ret_errors++;
						fprintf(stderr, "Option -e can accept only a character as delimiter.\n");
					} else {
						params->delimiter =  optarg[0];
						if(params->filename_format || params->str_format) {
							fprintf(stderr, "Warning: option -e should not be used together with -f or -F.\n");
						}
						if(params->delimiter == '%') {
							ret_errors++;
							fprintf(stderr, "Error: delimiter '%%' is not allowed.\n");
						}
					}
					break;

				case 'S':
					params->str_sql =  optarg;
					break;

				case 's':
					params->filename_sql =  optarg;
					break;

				case 'o':
					params->outputdir =  optarg;
					break;

				case 'm':
					params->flag_multifiles =  1;
					break;

				case 'a':
					params->flag_appendfile =  1;
					break;

				case 'g':
					params->flag_debugmsg =  1;
					break;

				case 'x':
					params->flag_debugmsg =  1;
					params->debugpref = optarg;
					break;

				case 'H':
					mysql_printf_usage(long_options);
					exit (1);
					break;

				case 'V':
					mysql_printf_version();
					mysql_printf_author_support();
					exit (1);
					break;

				case '?':
					/* getopt_long already printed an error message. */
					ret_errors++;
					break;

				default:
					mysql_printf_usage(long_options);
					exit (1);
					break;
			}
		}
	}

	/* Print any remaining command line arguments (not options). */
	if (optind < argc)
	{
		ret_errors += optind;

		fprintf(stderr, "non-option ARGV-elements: ");
		while (optind < argc) {
			fprintf(stderr, "%s ",
					STR_NULL(argv[optind]));
			optind++;
		}
		putchar ('\n');
	}

	return ret_errors;
}

void mysql_printf_author_support() {
	printf("\n\
Matteo Quintiliani - Istituto Nazionale di Geofisica e Vulcanologia - Italy\n\
Mail bug reports and suggestions to <%s>.\n",
			STR_NULL(PACKAGE_BUGREPORT)
			);
}

void mysql_printf_help_info() {
	printf("\n\
  Run with option --help for printing help information.\n\
			");
}


void mysql_printf_version() {
	printf("\
%s %s  ",
			STR_NULL(PACKAGE_NAME), STR_NULL(PACKAGE_VERSION)
			);

	mysql_printf_supports();

	printf("\n\
  A mysql client command line tool for formatting query results\n\
  by printf-like string format\n");

	printf("\n\
  Former development of mysql_printf was called 'mysql_format'.\n\
  You can run it using indifferently both names: mysql_printf or mysql_format.\n");

}


void mysql_printf_supports() {
	printf("(enabled features: libconfig ");
#ifdef HAVE_LIBCONFIG
	printf("YES");
#else
	printf("NO");
#endif
	printf(")\n");
}


void mysql_printf_usage(struct option long_options[]) {

	mysql_printf_version();

	printf("\n\
  Main usage in short: %s\n",
		STR_NULL(PACKAGE_NAME));

#ifdef HAVE_LIBCONFIG
	printf("\
    (\n\
    [ -C fileconf ] [ -D dbconfname ]\n\
    |\n\
      -h hostname -d dbname [ -u username ] [ -p password ] [ -P dbport ]\n\
    )\n");
#else
	printf("\
      -h hostname -d dbname [ -u username ] [ -p password ] [ -P dbport ]\n");
#endif

	printf("\
    ( -S SqlQueryString     | -s SqlQueryFileName )\n\
    ( -F PrintfFormatString | -f PrintfFormatFileName | -e delimiter )\n");


	printf("\
      [ -g | -e \"prefix\" ] [ -a ] [ -m ] [ -o <outputdir> ]\n");

	printf("\n\
mysql_printf is a mysql client command line tool for formatting query results\n\
by printf-like string format.  The printf-like strings can contain conversion\n\
specifiers %%s to customize the indentation and position of the column values\n\
in the query results.\n\
\n\
You can arrange the values of a single query row as a list of Delimiter\n\
Separated Values (i.e. CSV, comma separated values), or formatting them in a\n\
more complex way, for instance arranging the values of a single query row\n\
result in several customized lines.\n\
\n\
mysql_printf can be very useful to generate custom text files from database\n\
information.\n\
");

	printf("\
\n\
Without defining a printf-like string format, the default text output format is\n\
a list of delimiter-separated-values where delimiter is ';', but you can change\n\
with whatever character you want.\n\
\n\
mysql_printf handles all attribute values as strings, in fact it can accept\n\
only the printf conversion specifiers %%s. You can rely on powerful MySQL\n\
functions for pre-processing output fields format, mathematic computation or\n\
string concatenations.\n\
\n\
The conversion specifiers %%s within the printf-like strings have to be as many\n\
as the fields of the query result, except when using the option -m (see later).\n\
mysql_printf is also able to process the escape sequence, such as \\n, \\t, etc.\n\
\n");

	printf("\
Main arguments:\n\
-S, --sqlstr=STRING      SQL Query String.\n\
-s, --sqlfile=FILE       Filename containing SQL Query String.\n\
\n\
-F, --fmtstr=STRING      printf-like format string.\n\
-f, --fmtfile=FILE       File name containing printf-like format string.\n\
-e, --delimiter=C        Delimiter character between fields (default '%c').\n",
			DELIMITER_CHAR_DEFAULT);

#ifdef HAVE_LIBCONFIG
	printf("\
\n\
Arguments for libconfig:\n\
-C, --fileconf=FILE      DB configuration file (default %s).\n\
-D, --dbconfname=NAME    DB name to use from configuration file.\n",
			config_filename_default);
#endif

	printf("\
			\n\
Arguments for MySQL:\n\
-h, --host=name          Connect to MySQL host.\n\
-u, --user=name          MySQL User for login.\n\
-p, --password=name      Password to use when connecting to server.\n\
-d, --database=name      Data base name to connect.\n\
-P, --port=#             Port number to use for connection or 0 for default.\n");

	printf("\
\n\
Advanced arguments:\n\
-m, --multifiles         Create several files instead of formatting\n\
                         everything to the standard output.\n\
                         Each formatting of a single row of the query result\n\
                         will be redirected in a distinct file.\n\
                         The first attribute of the query result will\n\
                         assign the output file name.\n\
                         The output format string must contain one %%s less\n\
                         than number of attributes of query result.\n\
-a, --appendfile         With -m, each formatting of a row is appended\n\
                         to the output file.\n\
-o, --ouputdir=DIR       Output directory for output files.\n"
			);

	printf("\
\n\
Other arguments:\n\
-g, --debugmsg           Add debug messages in output as comment.\n\
                         Default prefix comment line is the character '%s'.\n\
                         You can change the prefix by option --debugpref.\n\
-x, --debugpref=string   Change prefix string for comment line.\n\
                         This option automatically enables option -g.\n\
-V, --version            Print tool version.\n\
-H, --help               Print this help.\n\
",
DEBUG_PREFIX_COMMENT
);

	mysql_printf_author_support();

}


int mysql_printf_check_params(MYSQL_FORMAT_PARAMS *params) {
	int ret = 0;

	if(params->filename_sql == NULL  &&  params->str_sql == NULL) {
		ret = -1;
		fprintf(stderr, "Argument <sqlfile> xor <sqlstr> is required!\n");
		/*
			 } else if(params->filename_format == NULL  && params->str_format == NULL) {
			 ret = -1;
			 fprintf(stderr, "<fmtfile> xor <str_format> is required!\n");
			 */
	} else if(params->filename_sql != NULL  &&  params->str_sql != NULL) {
		ret = -1;
		fprintf(stderr, "Only one argument between <sqlfile> or <sqlstr> can be defined, not together!\n");
		/*
			 } else if(params->filename_format != NULL  && params->str_format != NULL) {
			 ret = -1;
			 fprintf(stderr, "<fmtfile> or <str_format> can be define, not together!\n");
			 */
	} else if(params->outputdir != NULL) {
		if(!mysql_printf_dir_exists(params->outputdir)) {
			ret = -1;
			fprintf(stderr, "Output directory '%s' does not exist!\n", params->outputdir);
		}
	}

	return ret;
}


char *mysql_printf_gnu_getcwd () {
	size_t size = MAX_LEN_FILENAME;
	while (1)
	{
		char *buffer = (char *) malloc(size);
		if (getcwd (buffer, size) == buffer)
			return buffer;
		free (buffer);
		if (errno != ERANGE)
			return NULL;
		size *= 2;
	}
}

int mysql_printf_dir_exists (char *dirname) {
	int ret = 0;
	char *cur_dir = NULL;

	if(dirname) {
		cur_dir = mysql_printf_gnu_getcwd();
		if(chdir(dirname) == -1) {
			/* ERROR */
		} else {
			ret = 1;
		}
		if(cur_dir) {
			chdir(cur_dir);
			free(cur_dir);
		}
	}

	return ret;
}

